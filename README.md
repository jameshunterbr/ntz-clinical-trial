# NTZ Clinical Trial

Data and programs related to article "Nitazoxanide superiority to placebo to treat moderate COVID-19: A Pilot proof of concept randomized double-blind clinical trial".

The above files are intended to assist interested persons to review, test and use for their research the data for this clinical trial. We will be adding to the repository compendium files with our analytic programs/scripts (in R) to further assist users of the repository. If users require further data, we will be glad to make it available. Please get in touch with Dr. Hunter directly (jhunter@unifesp.br).
